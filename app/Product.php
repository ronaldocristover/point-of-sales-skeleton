<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use Uuids;

    public $incrementing = false;

   	public $fillable = ['name', 'operator_id', 'product_type_id','qty', 'buy_price', 'sell_price'];

   	public function operator()
   	{
   		return $this->belongsTo('App\Operator');
   	}

    public function productType()
    {
      return $this->belongsTo('App\productType');
    }

    public function sales()
    {
      return $this->hasMany('App\Sale');
    }

    public function purchase()
    {
      return $this->hasMany('App\Purchase');
    }
    
   	public static function getTotal(){
   		$totalData = DB::table('products')
   						->select(DB::raw("sum((qty*buy_price)) as total"))
   						->first();
   		return $totalData->total;
   	}

    public static function addStock($id, $qty)
    {
        $product = self::find($id);
        $product->qty = $product->qty + $qty;
        $product->save();
        return $product;
    }

    public static function reduceStock($id, $qty)
    {
        $product = self::find($id);
        $product->qty = $product->qty - $qty;
        $product->save();
        return $product;
    }
}
