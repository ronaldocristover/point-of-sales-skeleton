<?php

namespace App;

use DB;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    use Uuids;

    public $incrementing = false;

   	public $fillable = ['product_id', 'qty', 'buy_price', 'date'];

   	public function product()
   	{
   		return $this->belongsTo('App\Product');
   	}

   	public static function thisDay()
   	{
   		$data = DB::table('purchase')
   					->select(DB::raw('(buy_price*qty) as subtotal'))
   					->first();
   		return $data->subtotal;
   	}
}
