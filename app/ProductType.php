<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductType extends Model
{
    use Uuids;

    public $incrementing = false;

   	public $fillable = ['name'];

   	public function products(){
    	return $this->hasMany('App\Product');
    }
}
