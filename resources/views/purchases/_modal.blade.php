<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
             <form id="modal-form">
                {{ csrf_field() }}
                <input type="hidden" name="id" id="id">
                <input type="hidden" name="form-type" id="form-type" value="add">
              <div class="form-group">
                <label for="name">Tanggal:</label>
                {!! Form::date('date', \Carbon\Carbon::now(), [
                    'class' => 'form-control'
                ]) !!}
              </div>
              <div class="form-group">
                <label for="name">Product:</label>
                {!! Form::select('product_id', [''=>'']+App\Product::pluck('name','id')->all(), null, [
                    'class'=>'js-selectize',
                    'placeholder' => 'Pilih Produk']) !!}
              </div>
              <div class="form-group">
                <label for="name">Harga Beli Kemarin:</label>
                {!! Form::number('latest_buy_price', null, ['class'=>'form-control', 'min'=>0, 'readonly' => true]) !!}
              </div>
              <div class="form-group">
                <label for="name">Harga Beli Sekarang :</label>
                {!! Form::number('buy_price', null, ['class'=>'form-control', 'min'=>0]) !!}
              </div>
              <div class="form-group">
                <label for="name">Qty:</label>
                {!! Form::number('qty', null, ['class'=>'form-control', 'min'=>0]) !!}
              </div>
              
            </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-save">Simpan & Lanjutkan</button>
        <button type="button" class="btn btn-default btn-save">Simpan & Tutup</button>
      </div>
    </div>

  </div>
</div>
