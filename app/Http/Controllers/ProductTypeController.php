<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\ProductType;

use Yajra\Datatables\Datatables;

use Yajra\Datatables\Html\Builder;


class ProductTypeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request, Builder $htmlBuilder)
    {
        if ($request->ajax()) {
            $data = ProductType::all();
            return Datatables::of($data)
                ->addColumn('action', function($productType){
                    return view('datatables._action', [
                        'id' =>$productType->id,
                        'url' => url('product-type'),
                        'edit' => true,
                    ]);
                })->make(true);
        }

        $html = $htmlBuilder
            ->addColumn(['data' => 'name', 'name'=>'name', 'title'=>'Name'])
            ->addColumn(['data' => 'action', 'name'=>'action', 'title'=>'', 'orderable'=>false, 'searchable'=>false]);
        return view('product_types.index')->with(compact('html'));
    }

    public function store(Request $request)
    {
        if ($request->ajax()) {
            $data = ProductType::create([
                'name' => strtoupper($request->name),
                ]);
            return $data;
        }
    }
    public function update(Request $request)
    {
        if ($request->ajax()) {
            $data = ProductType::find($request->id);
            $data->name = strtoupper($request->name);
            $data->save();

            return $data;
        }
    }

    public function detail(Request $request)
    {
        if ($request->ajax()) {
            $id = $request->input('id');
            $data = ProductType::find($id);
            return $data;
        }
    }

    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {
            $data = ProductType::destroy($request->id);
            if ($data) {
                return $data;
            }
        }
    }
}
