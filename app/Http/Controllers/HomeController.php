<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['productTotal'] = 'Rp ' . number_format(\App\Product::getTotal());
        $data['daySale'] =  'Rp ' . number_format(\App\Sale::thisDay());
        return view('home', $data);
    }
}
