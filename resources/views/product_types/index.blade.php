@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><strong>Data Jenis Produk</strong></font>
                  &nbsp;<div class="btn btn-success btn-xs btn-add" style="margin-bottom: 10px;" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i></div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        {!! $html->table(['class'=>'table-striped']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('product_types._modal')
</div>

@endsection

@section('script')
{!! $html->scripts() !!}
<script>

    var formType = $("#form-type");
    var inputId = $("#id");
    var inputName = $("[name=name]");

    var modalTitle  = $(".modal-title");
    var btnSave = $(".btn-save");
    var _token = $("[name=_token]").val();


    // fetch data for datatables
    var data = '{{ url('product-type/data') }}';
     var oTable = $('.dt').DataTable({
        processing: true,
        serverSide: true,
        ajax: data,
            columns: [
                {data: 'id', name: 'id', orderable:false, searchable:false, visible:false},
                {data: 'name', name: 'name'},
                {data: 'action', name: 'action', orderable: false, searchable: false}

            ]
        });

     // when adding some data
    $(".btn-add").click(function() {
        modalTitle.html("Tambah Data");
        inputName.val("");

        btnSave.prop('disabled', false);
        formType.val("add");

    });

    // when click edit button
    $(document).on("click", ".edit-modal", function () {
        btnSave.prop('disabled', false);
         var id     = $(this).data('id');
         formType.val('update');
         $.ajax({
            dataType: "json",
            url: "{{ url('product-type/detail') }}",
            data: {
                id:id,
                _token: _token
            },
            type : "post",
            success: function(data){
                inputId.val(data.id);
                inputName.val(data.name);

                modalTitle.html('Update '+data.name);
            }
        });

    });

    // process data
    btnSave.click(function() {

        var me = $("#modal-form");
        btnSave.prop('disabled',true);
        btnSave.html('Sedang diproses..');
        url = "{{ route('product-type.store') }}";
        method = 'post';

        if(formType.val() != "add") {
            url = "{{ url('product-type/update') }}";
            method = 'put';
        }

        $.ajax({
            dataType: "json",
            url: url,
            data: me.serialize(),
            type :  method,
            success: function(data){
                btnSave.prop('disabled',false);
                btnSave.html('Simpan');
                swal({
                    type:"success",
                });
                window.LaravelDataTables["dataTableBuilder"].ajax.reload();
                $("#myModal").modal('toggle');

                // reset
                me[0].reset();
            },
             error: function(xhr, textStatus, errorThrown){
               swal({
                type:"error",
                html:errorThrown
            });
               btnSave.prop('disabled',false);
            }
        });

    });

    function deleteData(id){
        swal({
          title: 'Hapus Data!',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes'
        }).then(function () {
            processDelete(id)
        }, function (dismiss) {
          // dismiss can be 'cancel', 'overlay',
          // 'close', and 'timer'
          if (dismiss === 'cancel') {
            swal(
              'Cancelled',
              '',
              'error'
            )
          }
        })
    }

    function processDelete(id) {
        var url = '{{ url("product-type")}}' + '/' + id;
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            dataType: "json",
            url: url,
            type : "DELETE",
            data : {
                _token : $('meta[name="csrf-token"]').attr('content'),
                id : id
            },
            success: function(data){
                swal(
                  'Deleted!',
                  'Your file has been deleted.',
                  'success'
                )
                window.LaravelDataTables["dataTableBuilder"].ajax.reload();
            },
             error: function(xhr, textStatus, errorThrown){
               swal({
                type:"error",
                html:errorThrown
               });
            }
        });
    }
</script>
@endsection
