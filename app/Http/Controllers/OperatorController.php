<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Operator;

use Yajra\Datatables\Datatables;

use Yajra\Datatables\Html\Builder;


class OperatorController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request, Builder $htmlBuilder)
    {
        if ($request->ajax()) {
            $data = Operator::all();
            return Datatables::of($data)
                ->addColumn('action', function($operator){
                    return view('datatables._action', [
                        'id' =>$operator->id,
                        'url' => url('operator'),
                        'edit' => true,
                    ]);
                })->make(true);
        }

        $html = $htmlBuilder
            ->addColumn(['data' => 'name', 'name'=>'name', 'title'=>'Name'])
            ->addColumn(['data' => 'action', 'name'=>'action', 'title'=>'', 'orderable'=>false, 'searchable'=>false]);
        return view('operators.index')->with(compact('html'));
    }

    public function store(Request $request)
    {
        if ($request->ajax()) {
            $data = Operator::create([
                'name' => strtoupper($request->name),
                ]);
            return $data;
        }
    }
    public function update(Request $request)
    {
        if ($request->ajax()) {
            $data = Operator::find($request->id);
            $data->name = strtoupper($request->name);
            $data->save();

            return $data;
        }
    }

    public function detail(Request $request)
    {
        if ($request->ajax()) {
            $id = $request->input('id');
            $data = Operator::find($id);
            return $data;
        }
    }

    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {
            $data = Operator::destroy($request->id);
            if ($data) {
                return $data;
            }
        }
    }
}
