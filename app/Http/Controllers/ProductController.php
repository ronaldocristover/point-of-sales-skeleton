<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Product;
use Yajra\Datatables\Datatables;

use Yajra\Datatables\Html\Builder;


class ProductController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request, Builder $htmlBuilder)
    {
        if ($request->ajax()) {
            $data = Product::with('operator','productType');
            return Datatables::of($data)
                ->addColumn('action', function($productType){
                    return view('datatables._action', [
                        'id' =>$productType->id,
                        'url' => url('product'),
                        'edit' => true,
                    ]);

                })->make(true);
        }

        $html = $htmlBuilder
            ->addColumn(['data' => 'name', 'name'=>'name', 'title'=>'Name'])
            ->addColumn(['data' => 'buy_price', 'name'=>'buy_price', 'title'=>'Harga Beli'])
            ->addColumn(['data' => 'sell_price', 'name'=>'sell_price', 'title'=>'Harga Jual'])
            ->addColumn(['data' => 'qty', 'name'=>'qty', 'title'=>'Qty'])
            ->addColumn(['data' => 'action', 'name'=>'action', 'title'=>'', 'orderable'=>false, 'searchable'=>false]);
        return view('products.index')->with(compact('html'));
            return $html;
    }

    public function store(Request $request)
    {
        if ($request->ajax()) {
            $data = Product::create([
                'name' => strtoupper($request->name),
                'operator_id' => $request->operator_id,
                'product_type_id' => $request->product_type_id,
                'qty' => $request->qty,
                'buy_price' => $request->buy_price,
                'sell_price' => $request->sell_price,
            ]);
            return $data;
        }
    }
    public function update(Request $request)
    {
        if ($request->ajax()) {
            $data = Product::find($request->id);
            $data->name = strtoupper($request->name);
            $data->operator_id = $request->operator_id;
            $data->product_type_id = $request->product_type_id;
            $data->qty = $request->qty;
            $data->buy_price = $request->buy_price;
            $data->sell_price = $request->sell_price;
            $data->save();

            return $data;
        }
    }

    public function detail(Request $request)
    {
        // if ($request->ajax()) {
            $id = $request->input('id');
            $data = Product::find($id);
            return $data;
        // }
    }

    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {
            $data = Product::destroy($request->id);
            if ($data) {
                return $data;
            }
        }
    }
}
