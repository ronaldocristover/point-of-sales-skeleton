@extends('layouts.app')

@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Data Penjualan &nbsp; <div class="btn btn-success btn-xs btn-add" style="margin-bottom: 10px;" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Tambah</div>
                </div>
                <div class="panel-body">
                     {!! $html->table(['class'=>'table-striped']) !!}
                </div>
            </div>
        </div>
    </div>
</div>
    @include('sales._modal')
@endsection

@section('script')
{!! $html->scripts() !!}
<script>
    var formType = $("#form-type");
    var inputName = $("[name=name]");
    var inputId = $("#id");
    var modalTitle  = $(".modal-title");
    var btnSave = $(".btn-save");
    var _token = $("[name=_token]").val();
    var me = $("#modal-form");

     // when adding some data
    $(".btn-add").click(function() {
        modalTitle.html("Tambah Data");
        me[0].reset();

        btnSave.prop('disabled', false);
        formType.val("add");
    });

    // when click edit button
    $(document).on("click", ".edit-modal", function () {
        btnSave.prop('disabled', false);
         var id     = $(this).data('id');
         formType.val('update');
         $.ajax({
            dataType: "json",
            url: "{{ url('sale/detail') }}",
            data: {
                id:id,
                _token: _token
            },
            type : "post",
            success: function(data){
                $("[name=name]").val(data.name);
                $("[name=product_id]").val(data.operator_id);
                $("[name=qty]").val(data.qty);
                $("[name=buy_price]").val(data.buy_price);
                $("[name=sell_price]").val(data.sell_price);
                inputId.val(data.id);
                modalTitle.html('Update '+data.name);
            }
        });

    });

    // process data
    btnSave.click(function() {

        btnSave.prop('disabled',true);
        btnSave.html('Sedang diproses..');
        url = "{{ route('sale.store') }}";
        method = 'post';
        if(formType.val() != "add") {
            url = "{{ url('sale/update') }}";
            method = 'put';
        }

        $.ajax({
            dataType: "json",
            url: url,
            data: me.serialize(),
            type : method,
            success: function(data){
                btnSave.prop('disabled',false);
                btnSave.html('Simpan');
                swal({
                    type:"success",
                });
                window.LaravelDataTables["dataTableBuilder"].ajax.reload();
                $("#myModal").modal('toggle');

                // reset
               // reset
                me[0].reset();
            },
             error: function(xhr, textStatus, errorThrown){
               swal({
                type:"error",
                html:errorThrown
               });
               btnSave.prop('disabled',false);
            }
        });

    });

    function deleteData(id){
        swal({
          title: 'Hapus Data!',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes'
        }).then(function () {
            processDelete(id)
        }, function (dismiss) {
          // dismiss can be 'cancel', 'overlay',
          // 'close', and 'timer'
          if (dismiss === 'cancel') {
            swal(
              'Cancelled',
              '',
              'error'
            );
            btnSave.prop('disabled',false);
          }
        })
    }

    function processDelete(id) {
        var url = '{{ url("sale")}}' + '/' + id;
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            dataType: "json",
            url: url,
            type : "DELETE",
            data : {
                _token : $('meta[name="csrf-token"]').attr('content'),
                id : id
            },
            success: function(data){
                swal(
                  'Deleted!',
                  'Your file has been deleted.',
                  'success'
                )
                window.LaravelDataTables["dataTableBuilder"].ajax.reload();
            },
             error: function(xhr, textStatus, errorThrown){
               swal({
                type:"error",
                html:errorThrown
               });
            }
        });
    }

    // get product data
    // when click edit button
    $(document).on("change", "[name=product_id]", function () {
         var id     = $(this).val();
         alert("product id : " + id);
         $.ajax({
            dataType: "json",
            url: "{{ url('product/detail') }}",
            data: {
                id:id,
                _token: _token
            },
            type : "post",
            success: function(data){
                $("[name=buy_price]").val(data.buy_price);
                $("[name=sell_price]").val(data.sell_price);
            }
        });

    });
</script>
@endsection
