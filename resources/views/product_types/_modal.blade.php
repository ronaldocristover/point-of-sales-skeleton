<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
             <form id="modal-form">
                {{ csrf_field() }}
                <input type="hidden" name="id" id="id">
                <input type="hidden" name="form-type" id="form-type" value="add">
              <div class="form-group">
                <label for="name">Nama:</label>
                <input type="text" class="form-control" name="name">
              </div>
            </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-save">Simpan</button>
      </div>
    </div>

  </div>
</div>