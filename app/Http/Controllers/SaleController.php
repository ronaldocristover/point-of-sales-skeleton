<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Sale;

use Yajra\Datatables\Datatables;

use Yajra\Datatables\Html\Builder;


class SaleController extends Controller
{  
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request, Builder $htmlBuilder)
    {
        if ($request->ajax()) {
            $data = Sale::with('product');
            return Datatables::of($data)
                ->addColumn('subtotal', function($sale){
                    return number_format($sale->sell_price * $sale->qty);
                })
                ->addColumn('profit', function($sale){
                        $subtotal_beli = $sale->buy_price * $sale->qty;
                        $subtotal_jual = $sale->sell_price * $sale->qty;
                        return number_format($subtotal_jual - $subtotal_beli);
                })
                ->addColumn('action', function($sale){
                return view('datatables._action', [
                    'id' =>$sale->id,
                    'url' => url('product'),
                    'edit' => false,
                ]);

                })->make(true);
        }

        $html = $htmlBuilder
            ->addColumn(['data' => 'date', 'name'=>'date', 'title'=>'Tanggal'])
            ->addColumn(['data' => 'product.name', 'name'=>'product.name', 'title'=>'Produk'])
            ->addColumn(['data' => 'buy_price', 'name'=>'buy_price', 'title'=>'Harga Beli'])
            ->addColumn(['data' => 'sell_price', 'name'=>'sell_price', 'title'=>'Harga Jual'])
            ->addColumn(['data' => 'qty', 'name'=>'qty', 'title'=>'Qty'])
            ->addColumn(['data' => 'subtotal', 'name'=>'subtotal', 'title'=>'Subtotal'])
            ->addColumn(['data' => 'profit', 'name'=>'profit', 'title'=>'Profit'])
            ->addColumn(['data' => 'action', 'name'=>'action', 'title'=>'', 'orderable'=>false, 'searchable'=>false]);
        return view('sales.index')->with(compact('html'));
    }

    public function store(Request $request)
    {
        if ($request->ajax()) {
            $data = Sale::create([
                'product_id' => $request->product_id,
                'qty' => $request->qty,
                'buy_price' => $request->buy_price,
                'sell_price' => $request->sell_price,
                'date' => $request->date,
            ]);

            // add stock product
            $product = \App\Product::reduceStock($request->product_id, $request->qty);
            return $data;
        }
    }
    public function update(Request $request)
    {
        if ($request->ajax()) {
            $data = Sale::find($request->id);
            $data->product_id = $request->product_id;
            $data->qty = $request->qty;
            $data->buy_price = $request->buy_price;
            $data->sell_price = $request->sell_price;
            $data->date = $request->date;
            $data->save();

            return $data;
        }
    }

    public function detail(Request $request)
    {
        if ($request->ajax()) {
            $id = $request->input('id');
            $data = Sale::find($id);
            return $data;
        }
    }

    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {
            $data = Sale::destroy($request->id);
            if ($data) {
                return $data;
            }
        }
    }
}
