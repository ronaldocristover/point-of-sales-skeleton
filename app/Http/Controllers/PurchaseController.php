<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Purchase;

use Yajra\Datatables\Datatables;

use Yajra\Datatables\Html\Builder;


class PurchaseController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request, Builder $htmlBuilder)
    {
        if ($request->ajax()) {
            $data = Purchase::with('product');
            return Datatables::of($data)
                ->addColumn('subtotal', function($purchase){
                    return number_format($purchase->buy_price * $purchase->qty);
                })
                ->addColumn('action', function($purchase){
                return view('datatables._action', [
                    'id' =>$purchase->id,
                    'url' => url('purchase'),
                    'edit' => false,
                ]);

                })->make(true);
        }

        $html = $htmlBuilder
            ->addColumn(['data' => 'date', 'name'=>'date', 'title'=>'Tanggal'])
            ->addColumn(['data' => 'product.name', 'name'=>'product.name', 'title'=>'Produk'])
            ->addColumn(['data' => 'buy_price', 'name'=>'buy_price', 'title'=>'Harga Beli'])
            ->addColumn(['data' => 'qty', 'name'=>'qty', 'title'=>'Qty'])
            ->addColumn(['data' => 'subtotal', 'name'=>'subtotal', 'title'=>'Subtotal'])
            ->addColumn(['data' => 'action', 'name'=>'action', 'title'=>'', 'orderable'=>false, 'searchable'=>false]);
        return view('purchases.index')->with(compact('html'));
    }

    public function store(Request $request)
    {
        if ($request->ajax()) {
            $data = Purchase::create([
                'product_id' => $request->product_id,
                'qty' => $request->qty,
                'buy_price' => $request->buy_price,
                'date' => $request->date,
            ]);

            // add stock product
            $product = \App\Product::addStock($request->product_id, $request->qty);
            return $data;
        }
    }
    public function update(Request $request)
    {
    
    }

    public function show($id)
    {
        if ($request->ajax()) {
            $data = Purchase::find($id);
            return $data;
        }
    }

    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {
            $data = Purchase::destroy($request->id);
            if ($data) {
                return $data;
            }
        }
    }
}
