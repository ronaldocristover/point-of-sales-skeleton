<?php

namespace App;

use DB;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    use Uuids;

    public $incrementing = false;

   	public $fillable = ['product_id', 'qty', 'buy_price', 'sell_price', 'date'];

   	public function product()
   	{
   		return $this->belongsTo('App\Product');
   	}

   	public static function thisDay()
   	{
   		$data = DB::table('sales')
   					->select(DB::raw('(sell_price*qty) as subtotal'))
   					->first();
   		// return $data->subtotal;
            return $data;
   	}
}
