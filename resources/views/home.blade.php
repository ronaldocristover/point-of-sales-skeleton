@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h3>Halaman Utama</h3>
            <hr>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-xs-4">
                            <i class="fa fa-money fa-4x"></i>
                        </div>
                        <div class="col-xs-8 text-right">
                            <div>Modal Produk</div>
                            <div class="huge">{{ $productTotal }}</div>
                        </div>
                        </div>
                        
                    </div>

                </div>
            </div>

        </div>

        <div class="col-lg-3 col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-xs-4">
                            <i class="fa fa-money fa-4x"></i>
                        </div>
                        <div class="col-xs-8 text-right">
                            <div>Penjualan Hari ini</div>
                            <div class="huge">{{ $daySale }}</div>
                        </div>
                        </div>
                        
                    </div>

                </div>
            </div>
            
        </div>
    </div>
        </div>
</div>
@endsection
