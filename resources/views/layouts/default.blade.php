<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link href="{{ asset('css/font-awesome.min.css') }}" rel='stylesheet' type='text/css'>

    <link href="{{ asset('css/ionicons.min.css') }}" rel='stylesheet' type='text/css'>

    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

    <link href="{{ asset('css/jquery.dataTables.css') }}" rel="stylesheet">

    <link href="{{ asset('css/dataTables.bootstrap.css') }}" rel="stylesheet">

    <link href="{{ asset('css/selectize.css') }}" rel="stylesheet">

    <link href="{{ asset('css/selectize.bootstrap3.css') }}" rel="stylesheet">

    <link href="https://cdn.jsdelivr.net/sweetalert2/6.2.4/sweetalert2.min.css" rel="stylesheet">

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
    <div id="app">
        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/jquery-2.2.2.min.js') }}"></script>

    <script src="{{ asset('js/bootstrap.min.js') }}"></script>

    <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>

    <script src="{{ asset('js/dataTables.bootstrap.min.js') }}"></script>

    <script src="{{ asset('js/selectize.min.js') }}"></script>

    <script src="https://cdn.jsdelivr.net/sweetalert2/6.2.4/sweetalert2.min.js"></script>

    <script>
    // add selectize to select element
    $('.js-selectize').selectize({
      sortField: 'text'
    });
    </script>

    @yield('script')
</body>
</html>
