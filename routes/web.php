<?php
Auth::routes();

Route::get('/', 'HomeController@index');
Route::get('home', 'HomeController@index');
# Operator
Route::resource('operator', 'OperatorController');
Route::post('operator/detail', 'OperatorController@detail');

# Product Type
Route::resource('product-type', 'ProductTypeController');
Route::post('product-type/detail', 'ProductTypeController@detail');

# Product
Route::resource('product', 'ProductController');
Route::post('product/detail', 'ProductController@detail');

# Sale
Route::resource('sale', 'SaleController');
Route::post('sale/detail', 'SaleController@detail');

# Purchase
Route::resource('purchase', 'PurchaseController');
