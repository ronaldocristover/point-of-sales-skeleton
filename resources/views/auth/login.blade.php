@extends('layouts.default')

@section('content')
<style>
.login-section{
  padding: 100px;
}
.login-text{
    font-size: 25px;
    margin-top: 20px;
}
</style>
<div class="container">
    <div class="row">
        <div class="login-section">
            <div class="col-md-4">
            <div class="panel panel-default login-panel">
                <div class="panel-heading text-center">LOGIN</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">

                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus placeholder="Email">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">

                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control" name="password" required placeholder="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 ">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember"> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                <a class="btn btn-link" href="{{ url('/password/reset') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="wrapper">
                <div class="panel panel-default">
                <div class="panel-body">
                    <div class="col-md-4 text-center">
                        <img src="{{ asset('images/retail.png') }}" alt="Retail Images png">    
                    </div>
                    <div class="col-md-8" style="margin-top: 10px;margin-bottom: 10px; padding-left: 0px;">
                        <div class="login-text text-center" >
                                Point Of Sales Application
                            </div>    
                    </div>
                </div>
            </div>
            </div>
        </div>
        </div>
    </div>
</div>
@endsection
