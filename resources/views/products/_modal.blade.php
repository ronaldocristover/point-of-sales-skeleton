<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
             <form id="modal-form">
                {{ csrf_field() }}
                <input type="hidden" name="id" id="id">
                <input type="hidden" name="form-type" id="form-type" value="add">
              <div class="form-group">
                <label for="name">Nama:</label>
                <input type="text" class="form-control" name="name">
              </div>
              <div class="form-group">
                <label for="name">Operator:</label>
                {!! Form::select('operator_id', [''=>'']+App\Operator::pluck('name','id')->all(), null, [
                    'class'=>'js-selectize',
                    'placeholder' => 'Pilih Operator']) !!}
              </div>
              <div class="form-group">
                <label for="name">Jenis Produk:</label>
                {!! Form::select('product_type_id', [''=>'']+App\ProductType::pluck('name','id')->all(), null, [
                    'class'=>'js-selectize',
                    'placeholder' => 'Pilih Jenis Produk']) !!}
              </div>
              <div class="form-group">
                <label for="name">Qty:</label>
                {!! Form::number('qty', null, ['class'=>'form-control', 'min'=>0]) !!}
              </div>
              <div class="form-group">
                <label for="name">Harga Beli:</label>
                {!! Form::number('buy_price', null, ['class'=>'form-control', 'min'=>0]) !!}
              </div>
              <div class="form-group">
                <label for="name">Harga Jual:</label>
                {!! Form::number('sell_price', null, ['class'=>'form-control', 'min'=>0]) !!}
              </div>

            </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-save">Simpan</button>
      </div>
    </div>

  </div>
</div>
